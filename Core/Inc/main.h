/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define NC_Pin GPIO_PIN_14
#define NC_GPIO_Port GPIOC
#define NCC15_Pin GPIO_PIN_15
#define NCC15_GPIO_Port GPIOC
#define PWM1_Pin GPIO_PIN_0
#define PWM1_GPIO_Port GPIOA
#define PWM2_Pin GPIO_PIN_1
#define PWM2_GPIO_Port GPIOA
#define TEMP_ADC_Pin GPIO_PIN_2
#define TEMP_ADC_GPIO_Port GPIOA
#define DRV_RX_Pin GPIO_PIN_3
#define DRV_RX_GPIO_Port GPIOA
#define NCA4_Pin GPIO_PIN_4
#define NCA4_GPIO_Port GPIOA
#define PWR_MEAS_U1_Pin GPIO_PIN_5
#define PWR_MEAS_U1_GPIO_Port GPIOA
#define PWR_MEAS_I1_Pin GPIO_PIN_6
#define PWR_MEAS_I1_GPIO_Port GPIOA
#define PWR_MEAS_I2_Pin GPIO_PIN_7
#define PWR_MEAS_I2_GPIO_Port GPIOA
#define PWR_MEAS_U2_Pin GPIO_PIN_0
#define PWR_MEAS_U2_GPIO_Port GPIOB
#define NCB1_Pin GPIO_PIN_1
#define NCB1_GPIO_Port GPIOB
#define ILB_TX_ENABLE_Pin GPIO_PIN_8
#define ILB_TX_ENABLE_GPIO_Port GPIOA
#define ILB_TX_Pin GPIO_PIN_9
#define ILB_TX_GPIO_Port GPIOA
#define ILB_RX_Pin GPIO_PIN_10
#define ILB_RX_GPIO_Port GPIOA
#define NCA11_Pin GPIO_PIN_11
#define NCA11_GPIO_Port GPIOA
#define NCA12_Pin GPIO_PIN_12
#define NCA12_GPIO_Port GPIOA
#define LED_RED_Pin GPIO_PIN_4
#define LED_RED_GPIO_Port GPIOB
#define LED_GREEN_Pin GPIO_PIN_5
#define LED_GREEN_GPIO_Port GPIOB
#define NCB6_Pin GPIO_PIN_6
#define NCB6_GPIO_Port GPIOB
#define BOOT0_Charge_Pin GPIO_PIN_7
#define BOOT0_Charge_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
