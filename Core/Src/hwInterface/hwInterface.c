/*
 * hwInterface.c
 *
 *  Created on: Sep 15, 2021
 *      Author: A.Niggebaum
 */

#include "ilb.h"
#include "messageHandling.h"
#include "indicator.h"
#include "memory.h"

// STM32
#include "usart.h"
#include "tim.h"
#include "rng.h"
#include "gpio.h"
#include "adc.h"

#define CHA_PWMOUTPUT_CCR 	pwmOutputTimer->Instance->CCR2
#define CHB_PWMOUTPUT_CCR 	pwmOutputTimer->Instance->CCR1
#define PWM_OUTPUT_MAX		pwmOutputTimer->Instance->ARR

#define LOGIC_ERASE_NB_PAGES_TO_ERASE		2

TIM_HandleTypeDef* schedulerTimer = &htim22;			//!< 5ms timer for general handling of tasks and timeouts
TIM_HandleTypeDef* pwmOutputTimer = &htim2;			//!< PWM generating timer for outputs
TIM_HandleTypeDef* slow1sTimer	  = &htim7;				//!< timer running at 1s interval for long periods

//
//	Variables taken from linker script
//
extern uint32_t _interfacePage;			//!< page number where logic starts
extern uint32_t _LOGIC_ORIGIN;			//!< memory address of logic origin
extern uint32_t _pageSizeBytes;			//!< size of a page in bytes. Used to calculate how many pages need to be erased to delete logic

//
//	Private variables
//
static void (*ADCConversionCompleteCallback)(void) = NULL;		// callback to be called when the ADC channels have finished their measurement

//
//	Initialisation
//
/**
 * Initialisation function for all hardware, called by library
 * @return
 */
ILB_HAL_StatusTypeDef ILB_HAL_hardwareInit(void)
{
	// initialise memory
	memory_init();

	// start the PWM timer for the light engine interface
	HAL_TIM_PWM_Start(pwmOutputTimer, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(pwmOutputTimer, TIM_CHANNEL_2);

	// initialise the indicator interface
	indicator_init();

	// start timer for scheduler
	HAL_TIM_Base_Start_IT(schedulerTimer);
	// start the slow timer
	HAL_TIM_Base_Start_IT(slow1sTimer);

	// start the message receival process
	ILBStartMessageReceival();

	return ILB_HAL_OK;
}

//
//	UART communication
//
/**
 * Send bytes via the UART interface
 *
 * This routines is called from the library if it wants to send bytes. This function must be non blocking.
 * @param buffer
 * @param nOfBytes
 * @return
 */
ILB_HAL_StatusTypeDef ILB_HAL_UARTSendBytes(uint8_t* buffer, uint16_t nOfBytes)
{
	return ILBSendMessage(buffer, nOfBytes);
}

/**
 * Accessor for library if lines is busy.
 *
 * @return 0 if line is free
 */
uint8_t ILB_HAL_isLineBusy(void)
{
	return messageHandling_isBusBusy();
}

//
//	Timer
//
/**
 * Implementation of ST HAL timer callback
 * @param htim
 */
void HAL_TIM_PeriodElapsedCallback (TIM_HandleTypeDef *htim)
{
  if (htim == schedulerTimer) {
    // Scheduler timer has expired, set flag
	  lib_setSchedulerStatusWaiting();
	  // arm the indicator scheduler
	  indicator_arm();
  } else if (htim == slow1sTimer) {
	  lib_arm1sTimer();
  } else {
	  // forward to device
	  //ilbTimerCallback(htim);
  }
}

//
//	Hardware information
//
uint32_t ILB_HAL_getUID(void)
{
	return (HAL_GetUIDw0() ^ HAL_GetUIDw1() ^ HAL_GetUIDw2());
}

//
//	System reset
//
/**
 * Trigger a factory reset
 */
void ILB_HAL_systemReset(void)
{
	NVIC_SystemReset();
}

//
//	Memory interface
//
/**
 *
 * @param settings
 * @param version
 * @return
 */
ILB_HAL_StatusTypeDef ILB_HAL_loadLightEngineSettings(LightEngineSettings_t* settings, uint8_t version)
{
	return memory_readPWMLookupStructure(settings, version);
}

/**
 * Save the settings
 * @param settings
 * @return
 */
ILB_HAL_StatusTypeDef ILB_HAL_saveLightEngineSettings(LightEngineSettings_t* settings)
{
	return memory_writePWMLookupStructure(settings);
}

/**
 * Accessor to stored power cycle flags
 *
 * @return
 */
uint8_t ILB_HAL_getNumberOfPowerCycleFlags(void)
{
	return memory_getNumberOfResetFlags();
}

/**
 * Add a power cycle flag to memory
 */
void ILB_HAL_addPowerCycleFlag(void)
{
	memory_addPowerCycleFlag();
}

/**
 * Clear all power cycle flags from memory
 */
void ILB_HAL_clearPowerCycleFlags(void)
{
	memory_clearPowerCycleFlags();
}

//
//	Indicator access
//
void ILB_HAL_setIndicator(deviceIndicatorTypeDef indicatorDef, uint8_t pattern1, uint8_t pattern2)
{
	indicator_set(indicatorDef, pattern1, pattern2);
}

//
//	Light Engine output
//
/**
 * Sets the light engine output. All values are in range [0:1000] (0.1% resolution)
 * @param targetPWMA	PWM target for channel A in units of 0.1%
 * @param targetPWMB	PWM target for channel B in units of 0.1%
 */
void ILB_HAL_core_setLightEngine(uint16_t targetPWMA, uint16_t targetPWMB)
{
	CHA_PWMOUTPUT_CCR = (targetPWMA * PWM_OUTPUT_MAX)/1000;
	CHB_PWMOUTPUT_CCR = (targetPWMB * PWM_OUTPUT_MAX)/1000;
}

/**
 * Accessor for curent PWM output
 * @param PWMA
 * @param PWMB
 */
void ILB_HAL_core_getLightEngineOutput(uint16_t* PWMA, uint16_t* PWMB)
{
	*PWMA = CHA_PWMOUTPUT_CCR;
	*PWMB = CHB_PWMOUTPUT_CCR;
}

//
//	Random number and time keeping
//
/**
 * Accessor to tick counter
 * @return
 */
uint32_t ILB_HAL_getTick(void)
{
	return HAL_GetTick();
}

/**
 * Delay function
 * @param delay
 */
void ILB_HAL_Delay(uint32_t delay)
{
	HAL_Delay(delay);
}

/**
 * Accessor to random nubmer generator
 * @param nr
 * @return
 */
ILB_HAL_StatusTypeDef ILB_HAL_GenerateRandomNumber(uint32_t* nr)
{
	return (ILB_HAL_StatusTypeDef)HAL_RNG_GenerateRandomNumber(&hrng, nr);
}

//
//	Logic loading, writing and erasing
//
static uint8_t flashUnlocked = 0;

/**
 * Erase the logic
 *
 * This function does erase the header of the logic. This causes the base software to
 * default into follower/PWM limit mode. The actual logic program will be overwritten when
 * the new software is loaded.
 *
 * @return ILB_HAL_OK on success, ILB_HAL_ERROR otherwise
 */
ILB_HAL_StatusTypeDef ILB_HAL_eraseLogic(void)
{
	// Unlock flash
	HAL_FLASH_Unlock();
	flashUnlocked = 1;
	// get page where interface starts
	uint32_t* startOfLogicPage = &_LOGIC_ORIGIN;
	// erase upwards of the page
	uint32_t error = 0;
	FLASH_EraseInitTypeDef eraseDef;
	eraseDef.PageAddress = (uint32_t)startOfLogicPage;
	eraseDef.NbPages = LOGIC_ERASE_NB_PAGES_TO_ERASE;
	eraseDef.TypeErase = FLASH_TYPEERASE_PAGES;
	HAL_FLASHEx_Erase(&eraseDef, &error);
	// lock again
	HAL_FLASH_Lock();
	flashUnlocked = 0;

	// check result
	if (error == 0xFFFFFFFF)
	{
		return ILB_HAL_OK;
	}
	// error occured
	return ILB_HAL_ERROR;
}

/**
 * Execute tasks to prepare the memory
 *
 * @return ILB_HAL_OK on success, ILB_HAL_ERROR on failure
 */
ILB_HAL_StatusTypeDef ILB_HAL_logicPrepareMemory(void)
{
	if (HAL_FLASH_Unlock() == HAL_OK)
	{
		flashUnlocked = 1;
		return ILB_HAL_OK;
	}
	return ILB_HAL_ERROR;
}

/**
 * Write a page to memory
 *
 * This function always expects a full page. The page may be filled with zeros.
 * Page 0 is the page of the interface!!
 *
 * @param pageNumber
 * @param pData
 * @return
 */
ILB_HAL_StatusTypeDef ILB_HAL_logicWritePage(uint16_t pageNumber, uint32_t* pData)
{
	// check if flash is unlocked and we can write
	if (flashUnlocked != 1)
	{
		return ILB_HAL_ERROR;
	}

	// calcualte start of page
	uint32_t logicPageIndex = (uint32_t)&_interfacePage;
	uint32_t pageStartAddress = FLASH_BASE + ((pageNumber + logicPageIndex)* FLASH_PAGE_SIZE);

	// erase the page before writing the new program
	uint32_t error = 0;
	FLASH_EraseInitTypeDef eraseDef;
	eraseDef.PageAddress = pageStartAddress;
	eraseDef.NbPages = 1;
	eraseDef.TypeErase = FLASH_TYPEERASE_PAGES;
	HAL_FLASHEx_Erase(&eraseDef, &error);
	// check for error erasing page
	if (error != 0xFFFFFFFF)
	{
		return ILB_HAL_ERROR;
	}
	// write page
	uint32_t bytesWritten = 0;
	uint32_t wordsWritten = 0;
	while (bytesWritten < FLASH_PAGE_SIZE)
	{
		if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, pageStartAddress + bytesWritten, pData[wordsWritten]) != HAL_OK)
		{
			return ILB_HAL_ERROR;
		}
		wordsWritten++;
		bytesWritten += 4;
	}

	return ILB_HAL_OK;
}

/**
 * Secure the logic
 *
 * @return
 */
ILB_HAL_StatusTypeDef ILB_HAL_logicSecureLogic(void)
{
	if (HAL_FLASH_Lock() == HAL_OK)
	{
		flashUnlocked = 0;
		return ILB_HAL_OK;
	}
	return ILB_HAL_ERROR;
}

//
//	EEPROM access
//
ILB_HAL_StatusTypeDef ILB_HAL_EEPROMInterface(uint8_t accessor, uint32_t offsetBytes, uint32_t* pVar)
{
	return memory_EEPROMInterface(accessor, offsetBytes, pVar);
}

/**
 * Erase the eeprom managed by the logic
 * @return
 */
ILB_HAL_StatusTypeDef ILB_HAL_EEPROMErase(void)
{
	return memory_eraseLogicEEPROM();
}

//
//	ADC reading
//
#define VREF_CAL							0x1FF80078		//!< location of calibration value
#define POWER_MEASUREMENT_ADC_MAX_VALUE		4095			//!< 12 bit resolution
#define NOMINAL_SUPPLY_VOLTAGE				3300			//!< 3.3V supply voltage

enum ADCChannelEnum {
	ADC_CHANNEL_TEMP,     //!< ADC_CHANNEL_TEMP
	ADC_CHANNEL_U1,       //!< ADC_CHANNEL_U1
	ADC_CHANNEL_I1,       //!< ADC_CHANNEL_I1
	ADC_CHANNEL_I2,       //!< ADC_CHANNEL_I2
	ADC_CHANNEL_U2,       //!< ADC_CHANNEL_U2
	ADC_CHANNEL_REFERECNE,//!< ADC_CHANNEL_REFERECNE
	ADC_CHANNEL_BUFF_SIZE,//!< ADC_CHANNEL_BUFF_SIZE
};
static uint32_t ADCChannelReadings[ADC_CHANNEL_BUFF_SIZE] = {0};
static ADCReadings_t* targetStruct;

/**
 * Start a measurement.
 *
 * @param pBuffer
 * @param buffSize
 * @param completeCallback
 * @return
 */
ILB_HAL_StatusTypeDef ILB_HAL_StartADCMeasuremnt(ADCReadings_t* pValues, void (*completeCallback)(void))
{
	ADCConversionCompleteCallback = completeCallback;
	targetStruct = pValues;
	return (ILB_HAL_StatusTypeDef)HAL_ADC_Start_DMA(&hadc, &ADCChannelReadings[0], ADC_CHANNEL_BUFF_SIZE);
}

/**
 * ADC conversion complete trigger
 *
 * @param hadc
 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc)
{

	if (targetStruct != NULL && ADCConversionCompleteCallback != NULL)
	{
		// calibrate against power supply changes
		// ! There seems to be an issue with reading the internal reference voltage. The read value depends on the values applied to the other ADC channels
		// this may be caused by a ground loop or other issue. In the meantime, since we have a stable supply, we can use the nominal supply voltage
		//uint16_t VrefCalFactor = (3 * 1000 * (*(uint16_t*)VREF_CAL))/ADCChannelReadings[ADC_CHANNEL_REFERECNE];		// this value corresponds to 3.0V, calibrated in units of mV
		uint16_t VrefCalFactor = NOMINAL_SUPPLY_VOLTAGE;

		targetStruct->I1 = (uint16_t)(((uint32_t)ADCChannelReadings[ADC_CHANNEL_I1] * (uint32_t)VrefCalFactor)/(uint32_t)POWER_MEASUREMENT_ADC_MAX_VALUE);
		targetStruct->U1 = (uint16_t)(((uint32_t)ADCChannelReadings[ADC_CHANNEL_U1] * (uint32_t)VrefCalFactor)/(uint32_t)POWER_MEASUREMENT_ADC_MAX_VALUE);
		targetStruct->I2 = (uint16_t)(((uint32_t)ADCChannelReadings[ADC_CHANNEL_I2] * (uint32_t)VrefCalFactor)/(uint32_t)POWER_MEASUREMENT_ADC_MAX_VALUE);
		targetStruct->U2 = (uint16_t)(((uint32_t)ADCChannelReadings[ADC_CHANNEL_U2] * (uint32_t)VrefCalFactor)/(uint32_t)POWER_MEASUREMENT_ADC_MAX_VALUE);
		targetStruct->T  = (uint16_t)(((uint32_t)ADCChannelReadings[ADC_CHANNEL_TEMP] * (uint32_t)VrefCalFactor)/(uint32_t)POWER_MEASUREMENT_ADC_MAX_VALUE);

		// call callback
		ADCConversionCompleteCallback();
	}
}
