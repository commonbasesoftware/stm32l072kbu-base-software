/*
 * chipProtection.c
 *
 *  Created on: 02.05.2022
 *      Author: A.Niggebaum
 *
 * This file implements the chip level protection functionality
 */
#include "main.h"
#include "ilb.h"

/**
 * Check the protection status of the chip. If the protection status does not correspond to the requested level,
 * it will be changed and the chip power cycled.
 * @param level		level 0: no protection, level 1: read with mass erase on set to 0; debug interface active, level 2 and higher: full protection, disabling debug interface
 * @return
 */
ILB_HAL_StatusTypeDef ILB_HAL_CheckChipProtection(uint8_t level)
{
	// get the current configuration of the option bytes
	FLASH_OBProgramInitTypeDef currentConfig;
	HAL_FLASHEx_OBGetConfig(&currentConfig);

	// get the target protection
	uint8_t targetProtection = OB_RDP_LEVEL0;
	switch (level)
	{
	case 0:
		// already set
		break;

	case 1:
		targetProtection = OB_RDP_LEVEL1;
		break;

	default:
		targetProtection = OB_RDP_LEVEL2;
		break;
	}

	// check if we need to adjust the protection level
	// if not. return
	if (currentConfig.RDPLevel == targetProtection)
	{
		return ILB_HAL_OK;
	}

	currentConfig.OptionType      = OPTIONBYTE_RDP;
	currentConfig.RDPLevel        = targetProtection;
	if (HAL_FLASH_OB_Unlock() != HAL_OK)
	{
		return ILB_HAL_ERROR;
	}
	if (HAL_FLASHEx_OBProgram(&currentConfig) == HAL_OK)
	{
		HAL_StatusTypeDef status = HAL_FLASH_OB_Launch();
		status |= HAL_FLASH_OB_Lock();
		return status;
	}

	HAL_FLASH_OB_Lock();
	return ILB_HAL_ERROR;
}

/**
 * Returns the currently active chip protection level.
 * @return Set level. 0xFF indicates that this feature is not implemented
 */
uint8_t ILB_HAL_GetChipProtection(void)
{
	// get the current configuration
	FLASH_OBProgramInitTypeDef currentConfig;
	HAL_FLASHEx_OBGetConfig(&currentConfig);

	switch (currentConfig.RDPLevel)
	{
	case OB_RDP_LEVEL0:
		return 0;

	case OB_RDP_LEVEL1:
		return 1;

	case OB_RDP_LEVEL2:
		return 2;

	default:
		return 0xFF;
	}
}
