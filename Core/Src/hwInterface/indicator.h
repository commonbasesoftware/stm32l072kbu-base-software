/*
 * indicator.h
 *
 *  Created on: 09.08.2021
 *      Author: A.Niggebaum
 */

#ifndef SRC_INTERFACE_INDICATOR_H_
#define SRC_INTERFACE_INDICATOR_H_

#include "ilb.h"
#include "gpio.h"

// defines for indicator LEDs
//GPIO_PinState indicatorSet = GPIO_PIN_RESET;
//GPIO_PinState indicatorReset = GPIO_PIN_SET;
#define INDICATOR_SET		GPIO_PIN_RESET
#define INDICATOR_RESET		GPIO_PIN_SET

void indicator_init(void);
void indicator_arm(void);
void indicator_executeTask(void);
void indicator_set(deviceIndicatorTypeDef indicatorDef, uint8_t pattern1, uint8_t pattern2);

#endif /* SRC_INTERFACE_INDICATOR_H_ */
