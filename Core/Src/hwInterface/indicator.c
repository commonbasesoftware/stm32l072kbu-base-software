/*
 * indicator.c
 *
 *  Created on: 09.08.2021
 *      Author: A.Niggebaum
 */

#include "gpio.h"
#include "ilb.h"
#include "indicator.h"
#include "ilbstd.h"

#define LED_TIMER_PRESCALER		25												//!< prescaler for the LED timer with respect to the scheduler timer
#define LED_NUMBER_OF_BITS		0x08											//!< number of bits in an LED cycle. In other words the word length of a LED pattern.

// The pattern is an 8 bit sequence, a 1 indicates that the LED is on, a 0 that is is off.
// Each bit is held for ::LED_TIEMR_PRESCALER x schedulerTimer.
static uint8_t redLEDPattern = 0;														//!< red led indication pattern
static uint8_t greenLEDPattern = 0;													//!< green led indication pattern
static uint8_t ledOneShotPattern = 0;													//!< one shot pattern. If set, the led pattern will be set to zero, after it has completed all 8 bits
static uint8_t ledPrescalerCounter = 0;												//!< running prescaler counter
static uint8_t ledBitCounter = 0;														//!< bit position counter for led indicator

typedef enum {
	INDICATOR_IDLE,		//!< indicates that the routine is waiting
	INDICATOR_EXECUTE	//!< indicates that the indicator routine should be executed.
} indicatorStatusTypeDef;
indicatorStatusTypeDef indicatorStatus = INDICATOR_IDLE;

/**
 * Arm the routine to execute
 */
void indicator_arm(void)
{
	indicatorStatus = INDICATOR_EXECUTE;
}

/**
 * Initialise the indicators
 */
void indicator_init(void)
{
	// defines for indicator LEDs
	HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, INDICATOR_RESET);
	HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, INDICATOR_RESET);
}

/**
 * Sets indicator patterns for both indicators separately
 *
 * Sets the indicator pattern according to the specification in ::deviceIndicatorTypeDef. Low priority patterns will only be applied if
 * the current pattern buffers are empty. Otherwise they will be ignored
 *
 * @param indicatorDef	definition of the pattern according to ::deviceIndicatorTypeDef
 * @param pattern1		pattern for the green led or indicator LED1
 * @param pattern2		pattern for the red led or indicator LED2
 */
void indicator_set(deviceIndicatorTypeDef indicatorDef, uint8_t pattern1, uint8_t pattern2)
{
	if ((IS_BIT_SET(indicatorDef, DEVICE_LOW_PRIO_GREEN_INDICATOR)) && (redLEDPattern == 0 && greenLEDPattern == 0)) {
		greenLEDPattern = pattern1;
		ledOneShotPattern |= (indicatorDef & DEVICE_GREEN_ONESHOT);
	}
	if ((IS_BIT_SET(indicatorDef, DEVICE_LOW_PRIO_RED_INDICATOR)) && (redLEDPattern == 0 && greenLEDPattern == 0)) {
		redLEDPattern = pattern2;
		ledOneShotPattern |= (indicatorDef & DEVICE_RED_ONESHOT);
	}
	// reset counter
	ledBitCounter = 0;
	ledPrescalerCounter = 0;
	// set "normal" indicator if required
	if (IS_BIT_SET(indicatorDef, DEVICE_GREEN_INDICATOR)) {
		greenLEDPattern = pattern1;
		ledOneShotPattern |= (indicatorDef & DEVICE_GREEN_ONESHOT);
	}
	else
	{
		greenLEDPattern = 0;
	}
	if (IS_BIT_SET(indicatorDef, DEVICE_RED_INDICATOR)) {
		redLEDPattern = pattern2;
		ledOneShotPattern |= (indicatorDef & DEVICE_RED_ONESHOT);
	}
	else
	{
		redLEDPattern = 0;
	}

	// check if we need to disable the indicators
	if (redLEDPattern == 0x00 && HAL_GPIO_ReadPin(LED_RED_GPIO_Port, LED_RED_Pin) == INDICATOR_SET) {
		HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, INDICATOR_RESET);
	}
	if (greenLEDPattern == 0x00 && HAL_GPIO_ReadPin(LED_GREEN_GPIO_Port, LED_GREEN_Pin) == INDICATOR_SET) {
		HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, INDICATOR_RESET);
	}
}

/**
 * Regular routine to control the indicators
 */
void indicator_executeTask(void)
{
	if (indicatorStatus != INDICATOR_EXECUTE)
	{
		return;
	}

	indicatorStatus = INDICATOR_IDLE;

	if ((greenLEDPattern | redLEDPattern) != 0) {
		if (ledPrescalerCounter == 0) {
			// count up
			ledPrescalerCounter++;
			// set led outputs
			if (IS_BIT_SET(greenLEDPattern, (1<<(7-ledBitCounter)))) {
				HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, INDICATOR_SET);
			} else {
				HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, INDICATOR_RESET);
			}
			if (IS_BIT_SET(redLEDPattern, (1<<(7-ledBitCounter)))) {
				HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, INDICATOR_SET);
			} else {
				HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, INDICATOR_RESET);
			}

		} else {
			ledPrescalerCounter++;
			if (ledPrescalerCounter >= LED_TIMER_PRESCALER) {
				ledPrescalerCounter = 0;
				ledBitCounter++;
				if (ledBitCounter >= LED_NUMBER_OF_BITS) {
					ledBitCounter = 0;
					// check reset condition
					if (IS_BIT_SET(ledOneShotPattern, DEVICE_RED_ONESHOT)) {
						redLEDPattern = 0;
						ledOneShotPattern &= ~DEVICE_RED_ONESHOT;
						// deactivate output
						HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, INDICATOR_RESET);
					}
					if (IS_BIT_SET(ledOneShotPattern, DEVICE_GREEN_ONESHOT)) {
						greenLEDPattern = 0;
						ledOneShotPattern &= ~DEVICE_GREEN_ONESHOT;
						// deactivate output
						HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, INDICATOR_RESET);
					}
				}
			}
		}
	}
}
