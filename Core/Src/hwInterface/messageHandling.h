/*
 * messageReceival.h
 *
 *  Created on: 09.08.2021
 *      Author: A.Niggebaum
 */

#ifndef SRC_INTERFACE_MESSAGERECEIVAL_H_
#define SRC_INTERFACE_MESSAGERECEIVAL_H_

#include "ilb.h"

void ILBStartMessageReceival(void);
void ILBByteReceiveISR_DMA(void);
ILB_HAL_StatusTypeDef ILBSendMessage(uint8_t* buffer, uint16_t nOfBytes);
uint8_t messageHandling_isBusBusy(void);

#endif /* SRC_INTERFACE_MESSAGERECEIVAL_H_ */
