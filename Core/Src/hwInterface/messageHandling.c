/*
 * messageReceival.c
 *
 *  Created on: 09.08.2021
 *      Author: A.Niggebaum
 */

#include "usart.h"
#include "gpio.h"
#include "ilb.h"

#define ILB_RX_BUFFER_DEPTH						64
#define ILB_RX_DMA_BUFFER_SIZE					128		//!< depth of DMA buffer

/**
 * Definitions for message parsing.
 */
#define ILB_MESSAGE_ADDRESS_BYTE_POSITION			0							//!< defines the byte index that contains the address byte
#define ILB_MESSAGE_LENGTH_BYTE_POSITION			1							//!< defines the byte index that contains the message length field
#define ILB_MESSAGE_TYPE_IDENT_BYTE_POSITION		2							//!< defines the byte index that contains the byte defining the message type and identifier
#define ILB_MESSAGE_PAYLOAD_START					3

#define ILB_MESSAGE_NOF_EXTRA_BYTES					4							//!< number of bytes extra to ILB messages other than payload

#define RX_TIMEOUT_COUNTER_THRESHOLD				100							//!< value before the line is freed

// Receiving is not required as special state, because we can receive our own message
typedef enum ilbBusStatusEnum{
  ILB_BUS_IDLE,                                                              	//!< ILB Bus is idle in RX direction
  ILB_BUS_RECEIVING,															//!< bus is receiving
  ILB_BUS_SENDING,                                                           	//!< IBL bus is sending bytes
//  ILB_BUS_COOLDOWN,																//!< message has been sent or received. We need to wait a cooldown time before the bus can be used again
  ILB_BUS_PROCESSING,															//!< indicates that the message is being processed
  ILB_BUS_ERROR																	//!< indicates that an error occured while sending
} ILBBusStatusTypeDef;
static volatile ILBBusStatusTypeDef ILBBusStatus = ILB_BUS_IDLE;                 //!< Status variable for ILB bus receiving

static uint8_t rxBuffer[ILB_RX_BUFFER_DEPTH] = {0};                              //!< Buffer for address and number of bytes to receive
static uint8_t rxBufferPos = 0;													 //!< position in buffer
static volatile uint8_t activeRXMessageLength = 0;                               //!< Length of currently received message in bytes
static volatile uint8_t rxTimeoutCounter = 0;									 //!< periodically counted up value and reset on byte receival. used to unblock a locked rx line
static uint8_t lastRXByteWasEscapeByte = 0;										 //!< marker if the last RX byte was an escape byte

// DMA based recieval
static uint8_t DMA_RXBuffer[ILB_RX_DMA_BUFFER_SIZE];
static uint16_t DMA_buffer_tail = 0;

// bus monitoring
#define ILB_BUS_INACTIVE	0
#define ILB_BUS_ACTIVE		(20 + ((*((uint32_t *)(UID_BASE + 0x14U))) & 0x0F))		//!< timeout for active bus. Default value plus (ideally) random offset to avoid collisions
static volatile uint8_t busIsActive = ILB_BUS_INACTIVE;

// uart interface
UART_HandleTypeDef* ilbUARTInterface = &huart1;			//!< uart communication interface for ILB

// reset counter: TODO: implement proper handling of errors
static uint32_t interfaceResetCounter = 0;
#define UART_ERRORS_WATCHED			(USART_ISR_ORE | USART_ISR_FE)
#define UART_ERRORS_CLEAR			(USART_ICR_ORECF | USART_ICR_FECF)

/**
 * Start receiving messages
 *
 * Initialises and prepares all the necessary interfaces
 */
void ILBStartMessageReceival(void)
{
	HAL_UART_DMAStop(ilbUARTInterface);
	// reset tracker
	DMA_buffer_tail = 0;
	rxBufferPos = 0;
	rxTimeoutCounter = 0;
	activeRXMessageLength = 0;
	lastRXByteWasEscapeByte = 0;
	ILBBusStatus = ILB_BUS_IDLE;

	// start the DMA process
	HAL_UART_Receive_DMA(ilbUARTInterface, &DMA_RXBuffer[0], ILB_RX_DMA_BUFFER_SIZE);

	// alternative mehtod to detect busy line: external interrupt
	// Enable interrupt on PA10 (ILB RX)
	EXTI->IMR  |= EXTI_IMR_IM10;		// enable interrupt
	EXTI->RTSR |= EXTI_RTSR_RT10;		// rising edge trigger detection
	EXTI->FTSR |= EXTI_FTSR_FT10;		// falling edge trigger detection
	NVIC_EnableIRQ(EXTI4_15_IRQn);		// enable the irq
	NVIC_SetPriority(EXTI4_15_IRQn, 3);	// set priority
}

/**
 * Handle to send a message into the ILB
 * @param buffer
 * @param nOfBytes
 * @return
 */
ILB_HAL_StatusTypeDef ILBSendMessage(uint8_t* buffer, uint16_t nOfBytes)
{
	ILBBusStatus = ILB_BUS_SENDING;
	HAL_GPIO_WritePin(ILB_TX_ENABLE_GPIO_Port, ILB_TX_ENABLE_Pin, GPIO_PIN_SET);
	return (ILB_HAL_StatusTypeDef)(HAL_UART_Transmit_IT(ilbUARTInterface, buffer, nOfBytes));
}

/**
 * Handle the Complete message
 */
void handleCompleteRXBuffer(void)
{
	ILBBusStatus = ILB_BUS_PROCESSING;
	// count position down again
	rxBufferPos -= 1;
	// checksum check
	uint8_t checksum = 0;
	for (uint8_t i = ILB_MESSAGE_TYPE_IDENT_BYTE_POSITION; i<rxBufferPos; i++) {
		checksum += rxBuffer[i];
	}
	if (checksum != rxBuffer[rxBufferPos]) {
		rxBufferPos = 0;
		ILBBusStatus = ILB_BUS_IDLE;
		return;
	}

	// place message in buffer
	lib_ilbMessageReceived(&rxBuffer[0], rxBufferPos);

	// reset
	rxBufferPos = 0;
	ILBBusStatus = ILB_BUS_IDLE;
}

/**
* This routine is called in the main event loop and reads all bytes ready in the DMA buffer
*/
void ILBByteReceiveISR_DMA(void)
{

	// check for errors
	if ((ilbUARTInterface->Instance->ISR & UART_ERRORS_WATCHED) != 0)
	{
		// clear errors
		ilbUARTInterface->Instance->ICR |= UART_ERRORS_CLEAR;

		// count up errors
		interfaceResetCounter++;

		// restart interface
		ILBStartMessageReceival();
		return;
	}

	// handle bus active
	if (busIsActive != ILB_BUS_INACTIVE)
	{
		// count down
		busIsActive--;
	}

	// check if bytes are in the buffer and they need to be processed
	// also don't process when we are sending
	if((ILB_RX_DMA_BUFFER_SIZE - ilbUARTInterface->hdmarx->Instance->CNDTR) == DMA_buffer_tail)
	{
		//if (ILBBusStatus == ILB_BUS_COOLDOWN || ILBBusStatus == ILB_BUS_RECEIVING)
		if (ILBBusStatus == ILB_BUS_RECEIVING)
		{
			// count up timeout
			rxTimeoutCounter++;
			if (rxTimeoutCounter >= RX_TIMEOUT_COUNTER_THRESHOLD)
			{
				rxTimeoutCounter = 0;
				ILBBusStatus = ILB_BUS_IDLE;
				return;
			}
		}
		return;
	}

	// if we are sending, we need to ignore the bytes if we are sending
	if (ILBBusStatus == ILB_BUS_SENDING)
	{
		DMA_buffer_tail = (ILB_RX_DMA_BUFFER_SIZE - ilbUARTInterface->hdmarx->Instance->CNDTR);
		rxTimeoutCounter = 0;
		// important: exit here
		return;
	}

	// set to receival
	ILBBusStatus = ILB_BUS_RECEIVING;

	// reset timeout timer
	rxTimeoutCounter = 0;

	uint8_t newByte;

	// push bytes to buffers
	while ((ILB_RX_DMA_BUFFER_SIZE - ilbUARTInterface->hdmarx->Instance->CNDTR) != DMA_buffer_tail)
	{
		// get new byte
		newByte = DMA_RXBuffer[DMA_buffer_tail];

		// increase counter
		DMA_buffer_tail++;
		// check overflow
		if (DMA_buffer_tail >= ILB_RX_DMA_BUFFER_SIZE)
		{
			DMA_buffer_tail = 0;
		}

		if (newByte == ILB_MESSAGE_START_BYTE)
		{
			rxBufferPos = 0;
			continue;
		}

		if (newByte == ILB_MESSAGE_ESCAPE_BYTE)
		{
			lastRXByteWasEscapeByte = 1;
			continue;
		}

		// write to buffer
		if (lastRXByteWasEscapeByte == 1)
		{
			rxBuffer[rxBufferPos] = ILB_MESSAGE_ESCAPE_OPERATION(newByte);
			lastRXByteWasEscapeByte = 0;
		}
		else
		{
			// write to buffer
			rxBuffer[rxBufferPos] = newByte;
		}
		// count up position in buffer
		rxBufferPos += 1;
		// check for overflow
		if (rxBufferPos >= ILB_RX_BUFFER_DEPTH) {
			// handle the message
			handleCompleteRXBuffer();
//			ILBBusStatus = ILB_BUS_COOLDOWN;
			rxBufferPos = 0;
			continue;
		}
		// check if the message satisfies the criteria of a complete message
		if (rxBufferPos >= 5 && rxBufferPos == rxBuffer[ILB_MESSAGE_LENGTH_BYTE_POSITION] + 2) {
			// handle message complete
			handleCompleteRXBuffer();
//			ILBBusStatus = ILB_BUS_COOLDOWN;
			rxBufferPos = 0;
			continue;
		}
	}
}

/**
 * Accessor if bus is idle
 *
 * @return 0 if line is idle, 1 otherwise
 */
uint8_t messageHandling_isBusBusy(void)
{
	if (ILBBusStatus == ILB_BUS_IDLE && busIsActive == ILB_BUS_INACTIVE)
	{
		return 0;
	}
	return 1;
}

/**
 * Indicate that the message has been send successfully
 * @param huart
 */
void HAL_UART_TxCpltCallback (UART_HandleTypeDef *huart)
{
	HAL_GPIO_WritePin(ILB_TX_ENABLE_GPIO_Port, ILB_TX_ENABLE_Pin, GPIO_PIN_RESET);
	// ILBBusStatus = ILB_BUS_COOLDOWN;
	ILBBusStatus = ILB_BUS_IDLE;
	lib_messageSendSuccess(ILB_HAL_OK);
}

/**
 * Implementation of GPIO external interrupt callbacks.
 *
 * !! This is the DIRECT implementation of the vector service routine
 *
 * Used to detect activity on the bus
 * @param GPIP_Pin
 */
void EXTI4_15_IRQHandler(void)
{
	// check if the monitoring pin is triggered
	if ((EXTI->PR & ILB_RX_Pin) == ILB_RX_Pin)
	{
		// set line active
		busIsActive = ILB_BUS_ACTIVE;

		// clear interrupt
		__HAL_GPIO_EXTI_CLEAR_IT(ILB_RX_Pin);
	}
#ifdef LIBRARY_LOGIC
	if ((EXTI->PR & LIGHT_INT_Pin) == LIGHT_INT_Pin)
	{
		// call handler
		HAL_GPIO_EXTI_Callback(LIGHT_INT_Pin);

		// clear interrupt
		__HAL_GPIO_EXTI_CLEAR_IT(LIGHT_INT_Pin);
	}
#endif
}

